#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "jpegImport.h"

#include <iostream>
#include <QLabel>
#include <QImageReader>

void readImage(const char* path, QImage& out) {
    encoded_info encoded = { 0,0,NULL };
    decoded_info decoded = { 0,0,NULL };

    MatrixXi red, green, blue ;
    jpegImport(path, &encoded, &decoded, &red, &green, &blue);

    int width  = decoded.width;
    int height = decoded.height;

    QImage image(width, height, QImage::Format_RGB32);
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            QRgb color = qRgb(red(y,x), green(y,x), blue(y,x));
            image.setPixel(x, y, color);
        }
    }
    out = image;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QLabel *l = new QLabel;
//    QImageReader reader("C:/Users/speite/Documents/workspace/image.png");
//    QImage image = reader.read();

    QImage image(0, 0, QImage::Format_RGB32);
//    readImage("C:/Users/speite/Documents/workspace/sik-demo/contrib/testImage.jpg", image);
    readImage("../sik-demo/contrib/testImage.jpg", image);

    l->setPixmap(QPixmap::fromImage(image));
//    l->setText("Mein Text");
    setCentralWidget(l);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionQuit_triggered()
{
    QWidget::close();
}

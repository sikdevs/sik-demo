#include <QObject>
#include <QtDataVisualization>
#include <QDebug>
#include <QString>
#include <QUrl>
#include <QMap>
#include <QVector>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QJSValue>
#include <QThread>
#include <iostream>
#include <trees/treeControl.h>
#include <trees/decisionTree.h>
#include <trees/gaussianMixtureTree.h>
#include <uiUtil.h>
#include <util.h>

namespace Ui
{
  namespace Trees{

    void TreeControl::setStatus(const char* message = "Idle", bool showSpinner = false){
      auto window = baseObject->findChild<QObject*>("statusLabel");
      QMetaObject::invokeMethod(window, "setStatus", Q_ARG(QVariant, QString(message)), Q_ARG(QVariant, showSpinner));
    }

    void TreeControl::onCsvFileSelected(const QVariant& obj, const QVariant& header){
      auto url = obj.value<QUrl>();
      setStatus("loading File...", true);
      auto containsHeader = qvariant_cast<bool>(header);
      data = Util::readCSV(url, containsHeader);
      if(containsHeader){
        dataHeader = Util::readCSVHeader(url);
      }else{
        dataHeader.clear();
      }
      
      if(dataHeader.length() <= 0){
        for(int i = 0; i < data.cols(); ++i){
          dataHeader.push_back(QString("%1").arg(i));
        }
      }
      QObject *classComboBox = baseObject->findChild<QObject*>("classColComboBox"); 
      classComboBox->setProperty("model", dataHeader);
      setStatus();
    }

    template<typename T>
    size_t TreeControl::calcMetrics(const T& tree, const Eigen::ArrayXXd& X_validation, const Eigen::ArrayXi& y_validation){
      size_t rows = X_validation.rows();
      size_t hit = 0;
      for(size_t i = 0; i < rows; ++i){
        if(tree->predict(X_validation.row(i)) == y_validation(i)){
          hit += 1;
        }
      }
      return hit;
    }

    void TreeControl::work(){
      auto busyIndicator = baseObject->findChild<QObject*>("busyIndicator");
      busyIndicator->setProperty("running", true);

      if(data.rows() > 0){
        setStatus("prepare fitting ...", true);
        int classIndex = json["classColIndex"].toInt();
        int k = 5;
        std::vector<std::vector<int>> skfs = SIK::Util::Evaluation::stratifiedKFoldSet(k, data.col(classIndex));
        std::vector<int> validate_set = skfs[0];
        std::vector<int> train_set;
        for(int i = 1; i < k; ++i){
          train_set.insert(train_set.end(), skfs[i].begin(), skfs[i].end());
        }
        X = Eigen::ArrayXXd(data.rows(), data.cols() -1);
        X << data.leftCols(classIndex), data.rightCols(data.cols() - classIndex - 1);
        Eigen::ArrayXXd X_validate = SIK::Util::EigenTools::indexedRowAccess(X, skfs[0].begin(), skfs[0].end());
        X = SIK::Util::EigenTools::indexedRowAccess(X, train_set.begin(), train_set.end());
        Eigen::ArrayXi y_ = data.col(classIndex).cast<int>();
        std::for_each(y_.data(), y_.data()+y_.rows(), [](int &n){ n--; });
        Eigen::ArrayXi y_validate = SIK::Util::EigenTools::indexedRowAccess(y_, skfs[0].begin(), skfs[0].end());
        y = SIK::Util::EigenTools::indexedRowAccess(y_, train_set.begin(), train_set.end());
      
        if(json["mode"] == "DT"){
          QJsonObject rejectOptionObject = json["rejectOption"].toObject();
          double max_depth = json["maxDepth"].toDouble();
          double min_impurity_gain = json["minImpurity"].toDouble();
          double N_min = json["minElements"].toDouble();
          bool prune = json["prune"].toBool();
          size_t hit = 0;
          setStatus("fitting dt ...", true);
          if(rejectOptionObject["minmax"].toBool()){
            minmaxSplitTree = new SIK::Trees::DecisionTree<SIK::Trees::MinmaxRejectionSplit>(max_depth, min_impurity_gain, N_min);
            minmaxSplitTree->fit(X, y);
            if(prune){
              minmaxSplitTree->prune(X, y);
            }
            hit = calcMetrics(minmaxSplitTree, X_validate, y_validate);
          }else if(rejectOptionObject["meanstd"].toBool()){
            gaussianSplitTree = new SIK::Trees::DecisionTree<SIK::Trees::GaussianRejectionSplit>(max_depth, min_impurity_gain, N_min);
            gaussianSplitTree->fit(X, y);
            if(prune){
              gaussianSplitTree->prune(X, y);
            }
            hit = calcMetrics(gaussianSplitTree, X_validate, y_validate);
          }else if(rejectOptionObject["mahalanobis"].toBool()){
            mahalanobisSplitTree = new SIK::Trees::DecisionTree<SIK::Trees::MahalanobisRejectionSplit>(max_depth, min_impurity_gain, N_min);
            mahalanobisSplitTree->fit(X, y);
            if(prune){
              mahalanobisSplitTree->prune(X, y);
            }
            hit = calcMetrics(mahalanobisSplitTree, X_validate, y_validate);
          }else{
            basicSplitTree = new SIK::Trees::DecisionTree<SIK::Trees::BasicSplit>(max_depth, min_impurity_gain, N_min);
            basicSplitTree->fit(X, y);
            if(prune){
              basicSplitTree->prune(X, y);
            }
            hit = calcMetrics(basicSplitTree, X_validate, y_validate);
          }
          QObject *treeSettings = baseObject->findChild<QObject*>("treeSettings"); 
          QMetaObject::invokeMethod(treeSettings, "setResults", Q_ARG(QVariant, int(hit)), Q_ARG(QVariant, int(y_validate.rows() - hit)));
          //qDebug() << "hit: " << hit << " miss: " << y_validate.rows() - hit;
          setStatus("dt fitted");
        }else{
          double max_depth = json["maxDepth"].toDouble();
          double N_min = json["minElements"].toDouble();
          double rho = json["rho"].toDouble();
          double K = json["nElements"].toDouble();
          setStatus("fitting gmt ...", true);
          gmt = new SIK::Trees::GaussianMixtureTree(max_depth, N_min, K);
          gmt->fit(data, rho);
          setStatus("gmt fitted");
        }
      }else{
        setStatus("invalid CSV file - try another one");
      }
    }

    void TreeControl::onRun(const QVariant& obj){
      QString js = qvariant_cast<QString>(obj);
      QJsonDocument doc = QJsonDocument::fromJson(js.toUtf8());
      json = doc.object();
      this->setPriority(QThread::Priority::HighPriority);
      if(!this->isRunning()){
        this->start();
      }
    }

    void TreeControl::init(const QObject& base){
      baseObject = &base;
      QObject *treeSettings = baseObject->findChild<QObject*>("treeSettings"); 
      base.connect(treeSettings, SIGNAL(csvFileSelected(QVariant, QVariant)), this, SLOT(onCsvFileSelected(QVariant, QVariant)));
      base.connect(treeSettings, SIGNAL(run(QVariant)), this, SLOT(onRun(QVariant)));
    }
  }
}

#include <sikapp.h>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QApplication>
#include <QDebug>
#include <cluster/clusterControl.h>

namespace Ui {

SIKApp::SIKApp(int &argc, char** argv) : qapp(argc, argv) {
    QQmlEngine engine;
    QQmlComponent component(&engine, QUrl::fromLocalFile("qml/Main.qml"));
    qDebug() << component.errors();
    QObject *object = component.create();
    clusterControl.init(*object);
    treeControl.init(*object);
    bovControl.init(*object);
    featureControl.init(*object);
    qapp.exec();
}
}

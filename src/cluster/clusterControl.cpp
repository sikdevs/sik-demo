#include <QObject>
#include <QtDataVisualization>
#include <QDebug>
#include <QString>
#include <QUrl>
#include <cluster/clusterControl.h>
#include <QMap>
#include <QVector>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QJSValue>
#include <iostream>
#include <cluster/gmm.h>
#include <cluster/kmeans.h>
#include <uiUtil.h>

namespace Ui
{
  namespace Cluster{


    void ClusterControl::setStatus(const char* message = "idle"){
      auto window = baseObject->findChild<QObject*>("statusLabel");
      QMetaObject::invokeMethod(window, "setStatus", Q_ARG(QVariant, QString(message)), Q_ARG(QVariant, message != "idle"));
    }

    void ClusterControl::onBoxChange(const QVariant& obj){
      //objectName: "xvarComboBox"
    }

    void ClusterControl::onCsvFileSelected(const QVariant& obj, const QVariant& header){
      auto url = obj.value<QUrl>();
      setStatus("loading File...");
      auto containsHeader = qvariant_cast<bool>(header);
      data = Util::readCSV(url, containsHeader);
      if(containsHeader){
        dataHeader = Util::readCSVHeader(url);
      }
      setStatus("Idle");
    }

    void ClusterControl::prepareVisualView(){ 
      QObject *x = baseObject->findChild<QObject*>("xvarComboBox"); 
      QObject *y = baseObject->findChild<QObject*>("yvarComboBox"); 
      QObject *z = baseObject->findChild<QObject*>("zvarComboBox"); 
      if(dataHeader.length() <= 0){
        for(int i = 0; i < data.cols(); ++i){
          dataHeader.push_back(QString("%1").arg(i));
        }
      }

      x->setProperty("model", dataHeader);
      y->setProperty("model", dataHeader);
      z->setProperty("model", dataHeader);

      if(model != nullptr){
        auto map = QMap<int, QList<QVariant>>();
        for(int i = 0; i < data.rows(); ++i){
          auto row = QJsonArray();
          for(int j = 0; j < data.cols(); ++j){
            row.push_back(data(i,j));
          }
          auto p = ((SIK::Cluster::KMeans*)model)->predict(data.row(i));
          map[p].push_back(row);
        }
        QStringList colors(QColor::colorNames());
        for(auto key : map.keys()){
          QVariant ret;
          QVariant arr(map[key]);
          QVariant k("cluster_" + key);
          QObject *v3d = baseObject->findChild<QObject*>("Visualization3d"); 
          QMetaObject::invokeMethod(v3d, "addSeries", Q_RETURN_ARG(QVariant, ret), Q_ARG(QVariant, k), Q_ARG(QVariant, arr), Q_ARG(QVariant, colors.takeFirst()));
        }
      }
    }

    void ClusterControl::onRun(const QVariant& obj){
      QString js = qvariant_cast<QString>(obj);
      QJsonDocument doc = QJsonDocument::fromJson(js.toUtf8());
      QJsonObject json = doc.object();
      if(data.rows() > 0){
        if(json["mode"] == "GMM"){
          model = new SIK::Cluster::GMM(json["nClusters"].toDouble());
          setStatus("fitting gmm ...");
          ((SIK::Cluster::GMM*)model)->fit(data, json["nIterations"].toDouble(), json["precision"].toDouble());
          setStatus("model fitted");
        }else{
          model = new SIK::Cluster::KMeans(json["nClusters"].toDouble());
          setStatus("fitting kmeans ...");
          ((SIK::Cluster::KMeans*)model)->fit(data, json["nIterations"].toDouble(), json["precision"].toDouble());
          qDebug() << json["initializedCenters"].toString();
          setStatus("model fitted");
        }
      }else{
        setStatus("invalid CSV file - try another one");
      }
    }

    void ClusterControl::onTabChanged(const QVariant& obj, const QVariant& visible){
      auto v = qvariant_cast<bool>(visible);
      if(v){
        auto object =  qvariant_cast<QObject *>(obj);
        auto title = qvariant_cast<QString>(object->property("title"));
        if(title == "Settings"){
          //prepareSettingsView();
        }else if(title == "Visualization"){
          prepareVisualView();
        }
        
      }
    }

    void ClusterControl::init(const QObject& base){
      baseObject = &base;
      QObject *clusterSettings = baseObject->findChild<QObject*>("clusterSettings"); 
      QObject *tabView = baseObject->findChild<QObject*>("clusterTabView"); 
      base.connect(clusterSettings, SIGNAL(csvFileSelected(QVariant, QVariant)), this, SLOT(onCsvFileSelected(QVariant, QVariant)));
      base.connect(clusterSettings, SIGNAL(run(QVariant)), this, SLOT(onRun(QVariant)));
      base.connect(clusterSettings, SIGNAL(onTabChanged(QVariant, QVariant)), this, SLOT(onTabChanged(QVariant, QVariant)));
    }
  }
}

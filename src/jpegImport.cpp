#include "turbojpeg.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "jpegImport.h"
#include <Eigen/Dense>
//#include <zlib.h>
//#include <png.h>


using namespace std;

int jpegdecode(encoded_info *encoded, decoded_info *decoded) {

    // Initiliasierung der Decoding.

    tjhandle jpeg = tjInitDecompress();

    if (jpeg == NULL) {

        cout << tjGetErrorStr() << endl;
        return 1;
    };

    // Infos von Bilddatei, wie Breite und Höhe erhalten.

    int status = tjDecompressHeader(jpeg,
                                    (unsigned char *)encoded->data, encoded->size,
                                    &(decoded->width), &(decoded->height));
    if (0 != status) {

        cout << tjGetErrorStr() << endl;
        return 1;
    };

    // Erstellen der Buffer, die Infos des decoded Bilds speichert.

    decoded->data = malloc(sizeof(char) * 4 * decoded->width * decoded->height);

    // Decomression des Bildes.

    status = tjDecompress2(jpeg,
                           (unsigned char *)encoded->data, encoded->size,
                           (unsigned char *)decoded->data, 0, 0, 0,
                           TJPF_RGBA, 0);
    if (0 != status) {

        cout << tjGetErrorStr() << endl;
        return 2;
    };
    tjDestroy(jpeg);
    return 0;
}

//void read_png_file(const char* path, encoded_info* encoded, decoded_info* decoded) {

//    png_byte color_type;
//    png_byte bit_depth;

//    FILE *fp = fopen(path, "rb");

//    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
//    if (!png) abort();

//    png_infop info = png_create_info_struct(png);
//    if (!info) abort();

//    if (setjmp(png_jmpbuf(png))) abort();

//    png_init_io(png, fp);

//    png_read_info(png, info);

//    decoded->width = png_get_image_width(png, info);
//    decoded->height = png_get_image_height(png, info);
//    color_type = png_get_color_type(png, info);
//    bit_depth = png_get_bit_depth(png, info);

//    // Read any color_type into 8bit depth, RGBA format.
//    // See http://www.libpng.org/pub/png/libpng-manual.txt

//    if (bit_depth == 16)
//        png_set_strip_16(png);

//    if (color_type == PNG_COLOR_TYPE_PALETTE)
//        png_set_palette_to_rgb(png);

//    // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
//    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
//        png_set_expand_gray_1_2_4_to_8(png);

//    if (png_get_valid(png, info, PNG_INFO_tRNS))
//        png_set_tRNS_to_alpha(png);

//    // These color_type don't have an alpha channel then fill it with 0xff.
//    if (color_type == PNG_COLOR_TYPE_RGB ||
//        color_type == PNG_COLOR_TYPE_GRAY ||
//        color_type == PNG_COLOR_TYPE_PALETTE)
//        png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

//    if (color_type == PNG_COLOR_TYPE_GRAY ||
//        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
//        png_set_gray_to_rgb(png);

//    png_read_update_info(png, info);
//    png_uint_32 rowbytes = png_get_rowbytes(png, info);

//    decoded->data = malloc(sizeof(png_byte) * rowbytes * decoded->height);

//    png_bytep *rows = (png_bytep *)malloc(sizeof(png_bytep) * decoded->height);

//    for (png_uint_32 i = 0; i < (png_uint_32)decoded->height; ++i)
//        rows[i] = (png_byte *)(decoded->data) + i * rowbytes;

//    // FINALLY decode the image
//    png_read_image(png, rows);
//    free(rows);

//    fclose(fp);
//}

void jpegImport(const char* path, encoded_info* encoded, decoded_info* decoded,MatrixXi *rot, MatrixXi *gruen, MatrixXi *blau) {

    // Öffnen der Bilddatei.

    FILE * image = fopen(path, "rb");
    char* buffer;

    if (image == 0) {
        cout << "ERROR" << endl;
    }

    // Bestimmung der Dateigröße in byte.

    fseek(image, 0, SEEK_END);
    unsigned int a = ftell(image);

    // Wieder zurück am Dateianfang.

    rewind(image);

    // allocate memory to contain the whole file:
    buffer = (char*)malloc(sizeof(char)*a);
    // if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

    // Die Datei wird in buffer gespeichert.
    size_t x = fread(buffer, 1, a, image);

    // Die erhaltene Daten werden als Attributes von struct encoded eingegeben.

    encoded->data = buffer;
    encoded->size = a;
    encoded->pos = 0;

    //  Die Bilddatei wird geschlossen.

    fclose(image);

    // Die Funktion jpegdecode wird gerufen, um die Bild zu decoden, und ermittelte Daten werden
    // als Attributes von struct decoded eingegeben.

    int b = jpegdecode(encoded, decoded);

    // Falls es um eine PNG-Datei geht, wird die Funktion für PNG-Decoding gerufen.

    if (b == 1) {
//        read_png_file(path, encoded, decoded);
        std::cerr << "given file is not a jpeg file\n";
    };

    // Die Buffer enthält die Daten des encoded Bilds. Der Inhalt wird mit den Daten der decoded verändert.

    buffer = (char*)decoded->data;

    // Die Matrizen für die Pixeldateien werden deklariert

    MatrixXi rote(decoded->height, decoded->width), gruene(decoded->height, decoded->width), blaue(decoded->height, decoded->width);

    // Die Daten werden aus der Buffer gelesen und in den Matrizen gespeichert.

    for (int i = 0; i < decoded->height; i++) {
        for (int j = 0; j < decoded->width * 4; j += 4)
        {
            int xr = buffer[j + i * decoded->width * 4];
            int xg = buffer[j + i * decoded->width * 4 + 1];
            int xb = buffer[j + i * decoded->width * 4 + 2];

            // Compiler gibt "-1"(decimal) für 0xff hex-Wert. Ich habe bemerkt, dass ist der Fall
            // bei den manchen hex-Werte. Deswegen habe ich 256 addiert.

            if (xr < 0) {
                xr = 256 + xr;
            }
            if (xg < 0) {
                xg = 256 + xg;
            }
            if (xb < 0) {
                xb = 256 + xb;
            }

            // Die Pixelwerte werden in den entsprechenden Positionen der entsprechenden Matrizen gespeichert.

            rote(i, j / 4) = xr;
            gruene(i, j / 4) = xg;
            blaue(i, j / 4) = xb;
        }

    }

    *rot = rote;
    *gruen = gruene;
    *blau = blaue;

    // Freedom for the buffer

    free(buffer);
}


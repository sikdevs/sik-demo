#include <QObject>
#include <QApplication>
#include <QtDataVisualization>
#include <QDebug>
#include <QString>
#include <QUrl>
#include <QThreadPool>
#include <QMap>
#include <QVector>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QJSValue>
#include <featureselection/featureselect.h>
#include <Eigen/Dense>
#include <uiUtil.h>

#include <features/featureControl.h>


namespace Ui
{
  namespace Features{

    void FeatureControl::setStatus(const char* message = "Idle", bool showSpinner = false){
      auto window = baseObject->findChild<QObject*>("statusLabel");
      QMetaObject::invokeMethod(window, "setStatus", Q_ARG(QVariant, QString(message)), Q_ARG(QVariant, showSpinner));
    }

    void FeatureControl::work(){
      size_t numCandidates = 40;
      size_t iterations = 100;
      model = new SIK::Featureselection::CostSensitive();
      auto selections = model->select(features, classes, costs, numCandidates, iterations);
      QObject *featureSettings = baseObject->findChild<QObject*>("featureSettings"); 

      QMetaObject::invokeMethod(featureSettings, "clear");
      for(auto selection: selections){
        std::vector<int> featureIndices;
        QString lst;
        for(int idx : selection.featureIndices){
          lst = lst + "," + QString::number(idx);
        }
      QMetaObject::invokeMethod(featureSettings, "addSelection", 
          Q_ARG(QVariant, lst), 
          Q_ARG(QVariant, selection.merit), 
          Q_ARG(QVariant, selection.meanCost), 
          Q_ARG(QVariant, selection.costScatter)
          );
      }
      setStatus("costsensitive featureselection completed");
    }

      void FeatureControl::onRun(const QVariant& obj){
        QString js = qvariant_cast<QString>(obj);
        QJsonDocument doc = QJsonDocument::fromJson(js.toUtf8());
        settings = doc.object();
        if(costs.rows() > 0 && features.rows() > 0 && classes.rows() > 0){
          FeatureWorkerRunnable *worker = new FeatureWorkerRunnable(this);
          QThreadPool::globalInstance()->start(worker);
        }else{
          setStatus("error: check if the data and cost files were loaded properly");
        }
      }

      void FeatureControl::onDataFileSelected(const QVariant& obj, const QVariant& header){
        try{
          auto url = obj.value<QUrl>();
          setStatus("loading features file...");
          auto containsHeader = qvariant_cast<bool>(header);
          features = Util::readCSV(url, containsHeader);
          if(containsHeader){
            dataHeader = Util::readCSVHeader(url);
          }else{
            dataHeader.clear();
          }
          
          if(dataHeader.length() <= 0){
            for(int i = 0; i < features.cols(); ++i){
              dataHeader.push_back(QString("%1").arg(i));
            }
          }
          QObject *classComboBox = baseObject->findChild<QObject*>("featureClassesCB"); 
          classComboBox->setProperty("model", dataHeader);

          setStatus("data file loaded successful");
        } catch (const std::exception& ex) {
          setStatus("error: check your console for more informations");
          qDebug() << QString::fromStdString(ex.what());
        } catch (const std::string& ex) {
          setStatus("error: check your console for more informations");
          qDebug() << QString::fromStdString(ex);
        } catch (...) {
          setStatus("error while loading the features file");
        }
      }
      void FeatureControl::onCostFileSelected(const QVariant& obj, const QVariant& header){
        try{
          auto url = obj.value<QUrl>();
          setStatus("loading cost file...");
          auto containsHeader = qvariant_cast<bool>(header);
          costs = Util::readCSV(url, containsHeader);
          setStatus("cost file loaded successful");
        } catch (const std::exception& ex) {
          setStatus("error: check your console for more informations");
          qDebug() << QString::fromStdString(ex.what());
        } catch (const std::string& ex) {
          setStatus("error: check your console for more informations");
          qDebug() << QString::fromStdString(ex);
        } catch (...) {
          setStatus("error while loading the cost file");
        }
      }

      void FeatureControl::init(const QObject& base){
        baseObject = &base;
        QObject *featureSettings = baseObject->findChild<QObject*>("featureSettings"); 
        base.connect(featureSettings, SIGNAL(run(QVariant)), this, SLOT(onRun(QVariant)));
        base.connect(featureSettings, SIGNAL(dataFileSelected(QVariant, QVariant)), this, SLOT(onDataFileSelected(QVariant, QVariant)));
        base.connect(featureSettings, SIGNAL(costFileSelected(QVariant, QVariant)), this, SLOT(onCostFileSelected(QVariant, QVariant)));
      }
    }
  }

#include <QObject>
#include <QApplication>
#include <QtDataVisualization>
#include <QDebug>
#include <QString>
#include <QUrl>
#include <QThreadPool>
#include <QMap>
#include <QVector>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QJSValue>
#include <QImage>

#include <bov/unigram.h>
#include <bov/gmmunigram.h>
#include <bov/openworldunigram.h>
#include <Eigen/Dense>

#include <bov/bovControl.h>
#include <uiUtil.h>
#include <filters/filter.h>


namespace Ui
{
  namespace Bov{

    void BovControl::setStatus(const char* message = "Idle", bool showSpinner = false){
      auto window = baseObject->findChild<QObject*>("statusLabel");
      QMetaObject::invokeMethod(window, "setStatus", Q_ARG(QVariant, QString(message)), Q_ARG(QVariant, showSpinner));
    }

    void BovControl::work(){
      processImages();
      setStatus("fitting model...", true);
      if(settings["mode"] == "Uni"){
        model = new SIK::BOV::Unigram(settings["nWords"].toInt());
        ((SIK::BOV::Unigram*) model)->fit(data);
      }else if(settings["mode"] == "GMM"){
        model = new SIK::BOV::GMMUnigram(settings["nWords"].toInt());
        ((SIK::BOV::GMMUnigram*) model)->fit(data);
      }else{
        model = new SIK::BOV::OpenWorldUnigram(settings["nWords"].toInt());
        ((SIK::BOV::OpenWorldUnigram*) model)->fit(data, settings["rho"].toDouble());
      }
      setStatus("model fitted, start assignment...", true);
      assign(settings["assignment"] == "hard");
      setStatus();
    }

    void BovControl::processImages(){
      setStatus("process Images...", true);
      QDirIterator it(path.path(), QDir::Files, QDirIterator::Subdirectories);
      QVector<Eigen::ArrayXXd> dataEntries;
      int i = 0;

      int ctr = 1;
      while (it.hasNext()) {
        setStatus(QString(QString::number(ctr++) + " :> processing " + it.fileName()).toUtf8().constData());
        QString imagePath = it.next();
        Eigen::ArrayXXd imageData = Filter::processBovImage(imagePath);
        imagePaths << imagePath;
        dataEntries.push_back(imageData);
        i += imageData.rows();
      }
      data = Eigen::ArrayXXd(i, dataEntries[0].cols());
      setStatus("image processed, preparing preview...", true);
      QObject *bovSettings = baseObject->findChild<QObject*>("bovSettings"); 

      i = 0;
      ctr = 0;
      for(Eigen::ArrayXXd arr : dataEntries){
        int hist[256] = {0};
        int rows = arr.rows();
        for(int j = 0; j < rows; ++j){
          int v = arr(j, 3);
          if(v > 0 && v < 256){
            hist[v] += 1;
          }
        }

        QJsonArray jsArray;
        int k = 0;
        for(int d : hist){
          QJsonValue val = QJsonValue(d);
          jsArray.append(val);
        }
        QJsonDocument doc(jsArray);
        QString str = QString(doc.toJson());
        QMetaObject::invokeMethod(bovSettings, "addHistogramImage", Q_ARG(QVariant, imagePaths[ctr++]), Q_ARG(QVariant, str));
        data.block(i, 0, arr.rows(), arr.cols()) = arr;
        i += arr.rows();
        }
        
      }

      void BovControl::assign(const bool& hardAssignment){
      }

      void BovControl::onRun(const QVariant& obj){
        QString js = qvariant_cast<QString>(obj);
        QJsonDocument doc = QJsonDocument::fromJson(js.toUtf8());
        settings = doc.object();
        if(!path.isEmpty()){
          BovWorkerRunnable *worker = new BovWorkerRunnable(this);
          QThreadPool::globalInstance()->start(worker);
        }else{
          setStatus("path is not set yet.");
        }
      }

      void BovControl::onImagePathSelected(const QVariant& obj){
        path = qvariant_cast<QUrl>(obj);
      }

      void BovControl::onTabChanged(const QVariant& obj, const QVariant& visible){
        auto v = qvariant_cast<bool>(visible);
        if(v){
          auto object =  qvariant_cast<QObject *>(obj);
          auto title = qvariant_cast<QString>(object->property("title"));
        }
      }

      void BovControl::init(const QObject& base){
        baseObject = &base;
        QObject *bovSettings = baseObject->findChild<QObject*>("bovSettings"); 
        QObject *tabView = baseObject->findChild<QObject*>("tabView"); 
        base.connect(bovSettings, SIGNAL(run(QVariant)), this, SLOT(onRun(QVariant)));
        base.connect(bovSettings, SIGNAL(imgFileSelected(QVariant)), this, SLOT(onImagePathSelected(QVariant)));
      }
    }
  }

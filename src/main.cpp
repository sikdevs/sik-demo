#include <sikapp.h>
#include <filters/filter.h>
#include <turbojpeg.h>
#include <iostream>
#include <fstream>
#include <QString>
#include <Eigen/Dense>

int main(int argc, char *argv[])
{

    if(argc >= 2) {
        if(true) {
            QString in(argv[2]);
            QString out = QString::fromStdString(std::string(argv[2]) + std::string("_out.png"));
            Eigen::ArrayXXd image = Ui::Filter::loadImage(in);

            Eigen::ArrayXXd edge(3,1);
            edge << -1, 0, 1;
            Eigen::ArrayXXd blur(3,3);
            blur << 1, 2, 1,
                 2, 4, 2,
                 1, 2, 1;
            blur /= 16;
            Eigen::ArrayXXd filteredImage = Ui::Filter::filter(image, blur);
            Eigen::ArrayXXd dx = Ui::Filter::filter(filteredImage, edge);
            Eigen::ArrayXXd dy = Ui::Filter::filter(filteredImage, edge.transpose());
            filteredImage = (dx.pow(2) + dy.pow(2)).sqrt();
            Eigen::ArrayXd hist = Ui::Filter::ULBP(image);
            std::ofstream outfile (std::string(argv[2]) + std::string("_out.hist"),std::ofstream::binary);
            outfile << hist;
            outfile.close();
            Ui::Filter::saveImage(out, filteredImage);
        }
    } else {
        Ui::SIKApp app(argc, argv);
    }
    return 0;
}

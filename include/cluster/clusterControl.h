#pragma once

#include <QObject>
#include <QDebug>
#include <QUrl>
#include <Eigen/Dense>
#include <cluster/gmm.h>
#include <cluster/kmeans.h>
#include <uiUtil.h>

namespace Ui
{
  namespace Cluster{
    class ClusterControl : public QObject
    {
      Q_OBJECT

      public:
        const QObject* baseObject;
        Eigen::ArrayXXd data;
        QStringList dataHeader;
        ~ClusterControl(){
          if(model != nullptr){
            free(model);
          }
        };
        Eigen::ArrayXXd loadCSV(const QUrl& url);
        SIK::Cluster::Base *model = nullptr;
        void init(const QObject& base);
        void prepareVisualView();
        void prepareSettingsView();
        void setStatus(const char* message);

        public slots:
          void onCsvFileSelected(const QVariant& obj, const QVariant& header);
          void onBoxChange(const QVariant& obj);
          void onTabChanged(const QVariant& obj, const QVariant& visible);
          void onRun(const QVariant& obj);
    };
  }
}

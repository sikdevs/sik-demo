#pragma once
#include <Eigen/Dense>

using Eigen::MatrixXi;

struct encoded_info
{
    size_t size;
    size_t pos;
    void *data;
};

struct decoded_info
{
    int width;
    int height;
    void *data;
};
void jpegImport(const char* path, encoded_info* encoded, decoded_info* decoded, MatrixXi *rot, MatrixXi *gruen, MatrixXi *blau);
int jpegdecode(encoded_info *encoded, decoded_info *decoded);

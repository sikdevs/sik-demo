#pragma once

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QJsonObject>
#include <QUrl>
#include <Eigen/Dense>
#include <trees/decisionTree.h>
#include <trees/gaussianMixtureTree.h>
#include <uiUtil.h>

namespace Ui
{
  namespace Trees{
    class TreeControl : public QThread
    {
      Q_OBJECT

      void run() Q_DECL_OVERRIDE {
        work();
        emit resultReady();
      }

      public:
        const QObject* baseObject;
        Eigen::ArrayXXd data;
        Eigen::ArrayXXd X;
        Eigen::ArrayXi y;
        SIK::Trees::DecisionTree<SIK::Trees::BasicSplit> *basicSplitTree = nullptr;
        SIK::Trees::DecisionTree<SIK::Trees::MinmaxRejectionSplit> *minmaxSplitTree = nullptr;
        SIK::Trees::DecisionTree<SIK::Trees::GaussianRejectionSplit> *gaussianSplitTree = nullptr;
        SIK::Trees::DecisionTree<SIK::Trees::MahalanobisRejectionSplit> *mahalanobisSplitTree = nullptr;
        SIK::Trees::GaussianMixtureTree *gmt = nullptr;
        QJsonObject json;
        QStringList dataHeader;
        ~TreeControl(){
          //TODO: implement this correct
          if(basicSplitTree != nullptr){
            free(basicSplitTree);
          }
          if(minmaxSplitTree != nullptr){
            free(minmaxSplitTree);
          }
          if(gaussianSplitTree != nullptr){
            free(gaussianSplitTree);
          }
          if(mahalanobisSplitTree != nullptr){
            free(mahalanobisSplitTree);
          }
          if(gmt != nullptr){
            free(gmt);
          }
        };
        void init(const QObject& base);
        void setStatus(const char* message, bool showSpinner);
        void work();
        template<typename T>
        size_t calcMetrics(const T& tree, const Eigen::ArrayXXd& X_validation, const Eigen::ArrayXi& y_validation);

        public slots:
          void onCsvFileSelected(const QVariant& obj, const QVariant& header);
          void onRun(const QVariant& obj);
        signals:
          void resultReady();
    };
  }
}

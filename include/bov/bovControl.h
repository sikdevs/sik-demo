#pragma once

#include <QObject>
#include <QDebug>
#include <QUrl>
#include <QThread>
#include <QRunnable>
#include <QStringList>
#include <QJsonObject>
#include <Eigen/Dense>
#include <bov/model.h>
#include <uiUtil.h>

namespace Ui
{
  namespace Bov{
    class BovControl : public QObject
    {
      Q_OBJECT
      public:
        ~BovControl(){
          if(model != nullptr){
            free(model);
          }
        };
        const QObject* baseObject;
        SIK::BOV::Model *model = nullptr;
        QUrl path;
        QStringList imagePaths;
        QJsonObject settings;
        Eigen::ArrayXXd data;
        void init(const QObject& base);
        void prepareSettingsView();
        void processImages();
        void assign(const bool& hardAssignment);
        void work();
        void setStatus(const char* message, bool showSpinner);

        public slots:
        void onImagePathSelected(const QVariant& obj);
        void onTabChanged(const QVariant& obj, const QVariant& visible);
        void onRun(const QVariant& obj);
    };
    class BovWorkerRunnable : public QRunnable
    {
      BovControl *ref;
      public: BovWorkerRunnable(BovControl* ref) : ref(ref){
      }
      void run(){
        ref->work();
      }
    };
  }
}

#pragma once

#include <QObject>
#include <QDebug>
#include <QUrl>
#include <QThread>
#include <QRunnable>
#include <QStringList>
#include <QJsonObject>
#include <Eigen/Dense>
#include <featureselection/featureselect.h>
#include <uiUtil.h>
namespace Ui
{
  namespace Features{
    class FeatureControl : public QObject
    {
      Q_OBJECT
      public:
        ~FeatureControl(){
          if(model != nullptr){
            free(model);
          }
        };
        const QObject* baseObject;
        SIK::Featureselection::CostSensitive *model = nullptr;
        Eigen::ArrayXXd features;
        Eigen::ArrayXi classes;
        Eigen::ArrayXXd costs;
        QJsonObject settings;
        QStringList dataHeader;
        void init(const QObject& base);
        void setStatus(const char* message, bool showSpinner);
        void work();

        public slots:
        void onDataFileSelected(const QVariant& obj, const QVariant& header);
        void onCostFileSelected(const QVariant& obj, const QVariant& header);
        void onRun(const QVariant& obj);
    };
    class FeatureWorkerRunnable : public QRunnable
    {
      FeatureControl *ref;
      public: FeatureWorkerRunnable(FeatureControl* ref) : ref(ref){
      }
      void run(){
        ref->work();
      }
    };
  }
}

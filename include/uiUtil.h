#pragma once
#include <QUrl>
#include <QFile>
#include <QMap>
#include <QVector>
#include <Eigen/Dense>

namespace Ui
{
namespace Util
{
inline QStringList readCSVHeader(const QUrl& url) {
    QFile file(url.toLocalFile());
    if(file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);
        QStringList values;
        QString header = in.readLine();
        values = header.split(',');
        file.close();
        return values;
    }
    throw std::runtime_error("Error while getting CSV Header");
}
inline Eigen::ArrayXXd readCSV(const QUrl& url, const bool header) {
    QFile file(url.toLocalFile());
    if(file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);
        QVector<QStringList> values;
        if(header) {
            in.readLine();
        }
        while(!in.atEnd()) {
            QString line = in.readLine();
            values.push_back(line.split(','));
        }
        file.close();
        if(values.size() > 0) {
            QMap<int, QMap<QString, int>> nonNumbers;
            Eigen::ArrayXXd data(values.size(), values[0].size());
            int i=0, j=0;
            for(auto v : values) {
                j=0;
                bool ok;
                for(auto s : v) {
                    data(i, j) = s.toDouble(&ok);
                    if(!ok) {
                        if(nonNumbers[j].contains(s)) {
                            data(i, j) = nonNumbers[j][s];
                        } else {
                            nonNumbers[j][s] = nonNumbers[j].size();
                            data(i, j) = nonNumbers[j][s];
                        }
                    }
                    j++;
                }
                i++;
            }
            return data;
        }
    }

    Eigen::ArrayXXd data(0,0);
    return data;
}
}
}

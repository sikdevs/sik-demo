#pragma once

#include <QObject>
#include <QApplication>
#include <QDebug>
#include <QUrl>
#include <cluster/clusterControl.h>
#include <trees/treeControl.h>
#include <bov/bovControl.h>
#include <features/featureControl.h>

namespace Ui
{
class SIKApp
{
public:
    QApplication qapp;
    Cluster::ClusterControl clusterControl;
    Trees::TreeControl treeControl;
    Bov::BovControl bovControl;
    Features::FeatureControl featureControl;
    SIKApp(int &argc, char** argv);
    ~SIKApp() {};
};
}

#pragma once

#include <Eigen/Dense>
#include <QUrl>
#include <QImage>
#include <QColor>
#include <tuple>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>

namespace Ui
{
  namespace Filter
  {
    inline Eigen::ArrayXXd filter(const Eigen::ArrayXXd &image,
        const Eigen::ArrayXXd &filterMat)
    {
      assert(filterMat.rows() > 0 && filterMat.cols() > 0);
      assert(filterMat.rows() % 2 == 1 && filterMat.cols() % 2 == 1);

      int rows = image.rows();
      int cols = image.cols();
      int frows = filterMat.rows();
      int fcols = filterMat.cols();
      Eigen::ArrayXXd result(rows, cols);
      int r_ = std::floor(frows / 2.0);
      int c_ = std::floor(fcols / 2.0);

      for (int i = 0; i  < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
          //TODO: parallelize this
          for (int filter_x = 0; filter_x < frows; ++filter_x) {
            for (int filter_y = 0; filter_y < fcols; ++filter_y) {
              auto image_x = i + filter_x - r_;
              auto image_y = j + filter_y - c_;
              if (image_x > 0 && image_x < rows && image_y > 0 && image_y < cols) {
                result(i, j) += filterMat(filter_x, filter_y) * image(image_x, image_y);
              }
            }
          }
        }
      }

      return result;
    }

    inline void saveImage(const QString &path, const Eigen::ArrayXXd &img)
    {
      QImage image(img.rows(), img.cols(), QImage::Format_Grayscale8);

      for (int i = 0; i < img.rows(); ++i) {
        for (int j = 0; j < img.cols(); ++j) {
          int v = std::max(img(i, j), 0.0);
          auto c = QColor(v, v, v);
          image.setPixelColor(i, j, c);

        }
      }

      image.save(path);
    }

    inline Eigen::ArrayXXd loadImage(const QString &path)
    {
      QImage img(path);
      img = img.convertToFormat(QImage::Format_Grayscale8);
      Eigen::ArrayXXd image(img.width(), img.height());

      for (int i = 0; i < image.rows(); ++i) {
        for (int j = 0; j < image.cols(); ++j) {
          image(i, j) = qGray(img.pixel(i, j));
        }
      }

      return image;
    }
    inline Eigen::ArrayXXd loadImage(const QUrl &path)
    {
      return loadImage(path.toString());
    }


    inline Eigen::ArrayXd LBP_histogram(const Eigen::ArrayXXd &image, const int nNeighbors = 8, const int radius = 1, const int cellWidth = 16, const int cellHeight = 16){
      std::vector<Eigen::ArrayXd> histograms;
      int width = image.cols();
      int height= image.rows();

      int hcols = std::floor((double)width/cellHeight);
      int hrows = std::floor((double)height/cellWidth);
      double pi = std::atan(1)*4.0;

      std::vector<std::tuple<int,int>> neighborhood;
      for(int n = 0; n < nNeighbors; ++n){
        neighborhood.push_back(std::make_tuple(
              std::round(radius * std::cos(2.0*pi*n/nNeighbors)),
              std::round(radius * std::sin(2.0*pi*n/nNeighbors))
              ));
      }

      for(int i = 0; i < hcols; ++i){
        for(int j = 0; j < hrows; ++j){
          Eigen::ArrayXd values((cellWidth - 1) * (cellHeight - 1));
          values.setZero();
          Eigen::ArrayXd histogram(std::pow(2,nNeighbors));
          histogram.setZero();
          for(int cx = 1; cx < cellWidth - 1; ++cx){
            for(int cy = 1; cy < cellHeight - 1; ++cy){
              int value = 0;
              for(auto n : neighborhood){
                //if(x+std::get<0>(n) > 0 && x+std::get<0>(n) < width && y+std::get<1>(n) > 0 && y+std::get<1>(n) < height){
                int x = cx + i*cellWidth;
                int y = cy + j*cellHeight;
                //std::cout << x << "/" << y << " " << std::get<0>(n) << " " << std::get<1>(n) << std::endl;
                if(image(y + std::get<0>(n), x + std::get<1>(n)) - image(y, x) >= 0){
                  value += 1;
                }
                //}
                value <<= 1;
              }
              value >>=1;
              values(cx * (cellWidth - 1) + cy) = value;
            }
          }
          for(int v = 0; v < values.rows(); ++v){
            histogram(values(v)) +=1;
          }
          histograms.push_back(histogram);
        }
      }
      Eigen::ArrayXd result = Eigen::ArrayXd::Zero(histograms.size() * std::pow(2,nNeighbors));
      for(size_t h = 0; h < histograms.size(); ++h){
        result.segment(h*histograms[h].size(), histograms[h].size()) = histograms[h];
      }

      return result;
    }

    inline Eigen::ArrayXXd LBP(const Eigen::ArrayXXd &image, const int nNeighbors = 8, const int radius = 1, const int cellWidth = 16, const int cellHeight = 16)
    {
      int width = image.cols();
      int height= image.rows();
      Eigen::ArrayXXd lbp_values(width, height);

      int hcols = std::floor((double)width/cellHeight);
      int hrows = std::floor((double)height/cellWidth);
      double pi = std::atan(1)*4.0;

      std::vector<std::tuple<int,int>> neighborhood;
      for(int n = 0; n < nNeighbors; ++n){
        neighborhood.push_back(std::make_tuple(
              std::round(radius * std::cos(2.0*pi*n/nNeighbors)),
              std::round(radius * std::sin(2.0*pi*n/nNeighbors))
              ));
      }

      for(int i = 0; i < hcols; ++i){
        for(int j = 0; j < hrows; ++j){
          Eigen::ArrayXd values((cellWidth - 1) * (cellHeight - 1));
          values.setZero();
          Eigen::ArrayXd histogram(std::pow(2,nNeighbors));
          histogram.setZero();
          for(int cx = 1; cx < cellWidth - 1; ++cx){
            for(int cy = 1; cy < cellHeight - 1; ++cy){
              int value = 0;
              for(auto n : neighborhood){
                //if(x+std::get<0>(n) > 0 && x+std::get<0>(n) < width && y+std::get<1>(n) > 0 && y+std::get<1>(n) < height){
                int x = cx + i*cellWidth;
                int y = cy + j*cellHeight;
                //std::cout << x << "/" << y << " " << std::get<0>(n) << " " << std::get<1>(n) << std::endl;
                if(image(y + std::get<0>(n), x + std::get<1>(n)) - image(y, x) >= 0){
                  value += 1;
                }
                //}
                value <<= 1;
              }
              value >>=1;
              lbp_values(i,j) = value;
            }
          }
        }
      }
      return lbp_values;
    }

    inline Eigen::ArrayXd ULBP(const Eigen::ArrayXXd &image, const int cellWidth = 16, const int cellHeight = 16)
    {
      const int nNeighbors = 8;
      const int radius = 1;
      const std::vector<int> uniformes{0, 1, 2, 3, 4, 6, 7, 8, 12, 14, 15, 16, 24, 28, 30, 31, 32, 48, 56, 60, 62, 63, 64, 96, 112, 120, 124, 126, 127, 128, 129, 131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254, 255};
      auto lbp = LBP_histogram(image, nNeighbors, radius, cellWidth, cellHeight);
      const int hsize = std::pow(2, nNeighbors);
      const int n = lbp.rows() / hsize;

      Eigen::ArrayXd result(59*n);

      for(int i = 0; i < n; ++i){
        Eigen::ArrayXd histogram = lbp.segment(i * hsize, hsize);
        Eigen::ArrayXd nHist(59);
        nHist.setZero();
        int c = 1;
        for(int j = 0; j < hsize; ++j){
          if(std::find(std::begin(uniformes), std::end(uniformes), j) == uniformes.end()){
            nHist(c) = histogram(j);
          }else{
            nHist(0) += histogram(j);
          }
        }
        result.segment(i*59, 59) = nHist;
      }
      return result;
    }

    inline Eigen::ArrayXXd processBovImage(const QString& path){
      QImage img(path);
      //img = img.convertToFormat(QImage::Format_Grayscale8);
      Eigen::ArrayXXd red = Eigen::ArrayXXd::Zero(img.width(), img.height());
      Eigen::ArrayXXd green = Eigen::ArrayXXd::Zero(img.width(), img.height());
      Eigen::ArrayXXd blue = Eigen::ArrayXXd::Zero(img.width(), img.height());
      Eigen::ArrayXXd gray = Eigen::ArrayXXd::Zero(img.width(), img.height());
      std::vector<std::tuple<int,int>> skip;

      for (int i = 0; i < gray.rows(); ++i) {
        for (int j = 0; j < gray.cols(); ++j) {
          QRgb pixel = img.pixel(i,j);
          if(qAlpha(pixel) > 0){
            gray(i, j) = qGray(pixel);
            red(i, j) = qRed(pixel);
            green(i, j) = qGreen(pixel);
            blue(i, j) = qBlue(pixel);
          }else{
            skip.push_back(std::make_tuple(i,j));
          }
        }
      }
      Eigen::ArrayXXd edgeFilter(3,1);
      edgeFilter << -1, 0, 1;
      Eigen::ArrayXXd blurFilter(3,3);
      blurFilter <<  1, 2, 1,
                 2, 4, 2,
                 1, 2, 1;
      blurFilter /= 16;
      Eigen::ArrayXXd filteredGray = filter(gray, blurFilter);
      Eigen::ArrayXXd dx = filter(filteredGray, edgeFilter);
      Eigen::ArrayXXd dy = filter(filteredGray, edgeFilter.transpose());
      Eigen::ArrayXXd edgeImage = (dx.pow(2) + dy.pow(2)).sqrt();

      Eigen::ArrayXXd lbp = LBP(gray);
      Eigen::ArrayXXd imageData(gray.size(), 6);
      imageData.col(0) = Eigen::Map<Eigen::ArrayXd>(red.data(), red.size()).transpose();
      imageData.col(1) = Eigen::Map<Eigen::ArrayXd>(green.data(), green.size()).transpose();
      imageData.col(2) = Eigen::Map<Eigen::ArrayXd>(blue.data(), blue.size()).transpose();
      imageData.col(3) = Eigen::Map<Eigen::ArrayXd>(gray.data(), gray.size()).transpose();
      imageData.col(4) = Eigen::Map<Eigen::ArrayXd>(edgeImage.data(), edgeImage.size()).transpose();
      imageData.col(5) = Eigen::Map<Eigen::ArrayXd>(lbp.data(), lbp.size()).transpose();

      return imageData;
    }
  }
}

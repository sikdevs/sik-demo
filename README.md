# SIK - Smart Inspection Kit

Machine learning tools for automated visual inspection.

## Building

* Clone repository
* Adjust path to eigen headerfiles (line 9) and qt installation (line 32/33)s
* Open Project files / run `cd contrib/premake-qt && git submodule init && git submodule update`
* Obtain [premake5](http://premake.github.io/download.html)
* Generate build files (see [here](https://github.com/premake/premake-core/wiki/Using-Premake), e.g., `premake5 vs2013` for Visual Studio 2013 or `premake gmake` for GNU Make.
* Open Project files / run `make` / ...

## Third party libraries

Third party libraries are located in the `contrib` directory.


### [premake-qt](https://github.com/dcourtois/premake-qt)

Qt Addon for premake

No licenseinformation found :(

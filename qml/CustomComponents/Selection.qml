import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

RowLayout {
  id: selection
  property string selection: "";
  property double merit: 0;
  property double meanCost: 0;
  property double costScatter: 0;
  Label{text: "merit: " + selection.merit}
  Label{text: "mean cost: " + selection.meanCost}
  Label{text: "cost scatter: " + selection.costScatter}
  Label{text: "selection index: [" + selection.selection + "]" }
}

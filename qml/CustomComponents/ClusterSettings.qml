import QtQuick 2.7
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

import "." as Components

Item{
  id: clusterSettings
  objectName: "clusterSettings"

  signal csvFileSelected(var qurl, var header)
  signal onTabChanged(var obj, var visible)
  signal run(var data)

  function stringifySettings(gmmModelRB, nClustersTF, nIterationsTF, precisionTF){
    var settings = {};
    settings.mode = gmmModelRB.checked ? "GMM" : "KMeans";
    settings.nClusters = parseInt(nClustersTF.text);
    settings.nIterations = parseInt(nIterationsTF.text);
    settings.precision = parseFloat(precisionTF.text);
    return JSON.stringify(settings);
  }

  states: [
    State{
      name: "GMM"
      when: kMeansRB.checked
    },
    State{
      name: "KMeans"
      when: gmmRB.checked
    }
  ]

  anchors.fill:parent
  anchors.margins: 10

  TabView{
    id: clusterTabView
    objectName: "clusterTabView"

    anchors.fill: clusterSettings
    Tab{
      id: settingsTab
      title: "Settings"
      onVisibleChanged: {
        clusterSettings.onTabChanged(this, visible)
      }
      GridLayout{
        id: gridLayout
        columns: 3
        GroupBox{
          title: "Clustermode"
          anchors.top: parent.top
          ColumnLayout{
            ExclusiveGroup { id: clusterMode }
            RadioButton {
              text: "K-Means"
              id: kMeansRB
              checked: true
              exclusiveGroup: clusterMode
            }
            RadioButton {
              text: "GMM"
              id: gmmRB
              signal csvFileSelected(var qurl, var header)
              exclusiveGroup: clusterMode
            }
          }
        }
        GroupBox{
          title: "Data"
          anchors.top: parent.top
          Components.CsvData{
            onCsvFileSelected: clusterSettings.csvFileSelected(qurl, header)
          }
        }
        GroupBox{
          title: "Settings"
          anchors.top: parent.top
          GridLayout{
            columns: 2
            Label{text: "n Clusters: "}
            TextField{id: nclustersTF; text: "2"; validator: intsOnly}
            Label{text: "n iterations: "}
            TextField{id: niterationsTF; text: "10"; validator: intsOnly}
            Label{text: "precision: "}
            TextField{id: precisionTF; text: "0.001"; validator: numbersOnly}
          }
        }
        Button{
          id: "runClusterSettings"
          objectName: "runClusterSettings"
          text: "Run"
          onClicked:{
            clusterSettings.run(stringifySettings(gmmRB, nclustersTF, niterationsTF, precisionTF));
          }
        }
        RegExpValidator{
          id: intsOnly
          regExp: /[1-9]{1}[0-9]{0,10}/
        }
        DoubleValidator{
          id: numbersOnly
        }
      }
    }
    Tab {
      title: "Visualization"
      onVisibleChanged: clusterSettings.onTabChanged(this, visible)
      Components.Visualization3d{
        objectName: "Visualization3d"
      }
    }
  }
}

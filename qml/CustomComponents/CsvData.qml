import QtQuick 2.7
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

ColumnLayout{
  signal csvFileSelected(var qurl, var header)
  property bool selected
  CheckBox{
    id: csvHeaderCheckBox
    objectName: "csvHeader"
    text: "CSV-Header"
    onClicked: {
      if(selected){
        csvFileSelected(fileDialog.fileUrl, this.checked)
      }
    }
  }
  Button{
    text: "select"
    onClicked: fileDialog.visible = true
  }
  FileDialog {
    id: fileDialog
    title: "Please choose a file"
    folder: shortcuts.home
    nameFilters: ["csv files (*.csv)"]
    onAccepted: {
      selected=true
      csvFileSelected(Qt.resolvedUrl(this.fileUrl), csvHeaderCheckBox.checked)
    }
  }
}

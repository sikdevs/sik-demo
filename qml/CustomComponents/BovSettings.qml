import QtQuick 2.7
import QtQuick 2.3
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

import "." as Components

Item{
  id: bovSettings
  objectName: "bovSettings"
  anchors.fill: parent
  anchors.margins: 10

  signal imgFileSelected(var qurl)
  signal run(var data)

  function addHistogramImage(path, data){
    var li = {"path": path, "hist": data};
    bovListView.model.append(li);
  }
  function stringifySettings(){
    var settings = {};
    if(unigramRadioButton.checked){
      settings.mode = "Uni";
    }else if(owUnigramRadioButton.checked){
      settings.mode = "OW";
    }else if(gmmUnigramRadioButton.checked){
      settings.mode = "GMM";
    }
    settings.path = imageDataChooser.folder;
    settings.nWords = parseInt(bov_nWordsTextField.text);
    settings.assignment = bov_hardAssignmentRadioButton.checked ? "hard" : "soft";
    settings.rho = parseFloat(bov_rhoTextField.text);
    return JSON.stringify(settings);
  }
  states: [
    State{
      name: "Uni"
      when: unigramRadioButton.checked
      PropertyChanges{target: bov_rhoLabel; enabled: false}
      PropertyChanges{target: bov_rhoTextField; enabled: false}
    },
    State{
      name: "OW"
      when: owUnigramRadioButton.checked
      PropertyChanges{target: bov_rhoLabel; enabled: true}
      PropertyChanges{target: bov_rhoTextField; enabled: true}
    },
    State{
      name: "GMM"
      when: gmmUnigramRadioButton.checked
      PropertyChanges{target: bov_rhoLabel; enabled: false}
      PropertyChanges{target: bov_rhoTextField; enabled: false}
    }
  ]
  ColumnLayout{
    anchors.fill: parent
    GridLayout{
      id: grid
      columns: 3
      GroupBox{
        title: "BOV Mode"
        anchors.top: parent.top
        ColumnLayout{
          ExclusiveGroup { id: clusterMode }
          RadioButton {
            text: "Unigram"
            id: unigramRadioButton
            checked: true
            exclusiveGroup: clusterMode
          }
          RadioButton {
            text: "GMM Unigram"
            id: gmmUnigramRadioButton
            exclusiveGroup: clusterMode
          }
          RadioButton {
            text: "OpenWorld Unigram"
            id: owUnigramRadioButton
            exclusiveGroup: clusterMode
          }
        }
      }
      GroupBox{
        title: "Data"
        anchors.top: parent.top
        Components.ImgData{
          id: imageDataChooser;
          onImgFileSelected: {
            bovSettings.imgFileSelected(qurl);
          }
        }
      }
      GroupBox{
        title: "Settings"
        anchors.top: parent.top
        GridLayout{
          columns: 2
          Label{id: bov_nWordsLabel; text: "n Words: "}
          TextField{id: bov_nWordsTextField; text: "1"; validator: intsOnly}
          Label{id: bov_assignmentLabel; text: "Assignment: "}
          ExclusiveGroup { id: assignmentMode }
          RowLayout{
            RadioButton {
              text: "hard"
              id: bov_hardAssignmentRadioButton
              checked: true
              exclusiveGroup: assignmentMode
            }
            RadioButton {
              text: "soft"
              id: bov_softAssignmentRadioButton
              exclusiveGroup: assignmentMode
            }
          }
          Label{id: bov_rhoLabel; text: "rho: "}
          TextField{id: bov_rhoTextField; text: "0.85"; validator: numbersOnly;}
        }
      }
      Button{
        id: runTreeSettings
        objectName: "runTreeSettings"
        text: "Run"
        onClicked:{
          bovSettings.run(stringifySettings());
        }
      }
      RegExpValidator{
        id: jsonArrayOnly
        regExp: /\[(\[(-?(?=[1-9]|0(?!\d))\d+(\.\d+)?([eE][+-]?\d+)?,?)*\],?)*\]/
      }
      RegExpValidator{
        id: intsOnly
        regExp: /[1-9]{1}[0-9]{0,10}/
      }
      DoubleValidator{
        id: numbersOnly
      }
    }
    ScrollView{
      id: scrollView
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.alignment: Qt.AlignTop
      flickableItem.interactive: true
      contentItem: 
        ListView{
            id: bovListView;
            objectName: "bovListView";
            model: lstModel
            delegate: Components.HistogramImage{
              src: path;
              histogram: JSON.parse(hist);
              imageSize: 128;
        }
      }
    }
    ListModel {
      id: lstModel;
      //ListElement{path: "../../meta/bioID/rBioID_0000.pgm"; hist: "[1,2,3,4,5,6,7,8,9,0,10]"}
      //ListElement{path: "../../meta/bioID/rBioID_0001.pgm"; hist: "[1,2,3,4,5,6,7,8,9,0,10]"}
      //ListElement{path: "../../meta/bioID/rBioID_0002.pgm"; hist: "[1,2,3,4,5,6,7,8,9,0,10]"}
      //ListElement{path: "../../meta/bioID/rBioID_0003.pgm"; hist: "[1,2,3,4,5,6,7,8,9,0,10]"}
      //ListElement{path: "../../meta/bioID/rBioID_0004.pgm"; hist: "[1,2,3,4,5,6,7,8,9,0,10]"}
    }
  }
}

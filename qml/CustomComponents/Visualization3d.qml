import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0
import QtCharts 2.2
import QtDataVisualization 1.2

Item{
  width: 640
  height: 480
  id: visualization3d
  objectName: "Visualization3d"
  property variant series: []

  function addSeries(key, data, color){
    var values = '';
    data.forEach(function(row, index){
      values = values + ' ListElement{x:"'+ row[xvar.currentIndex] +'"; y:"'+ row[yvar.currentIndex] + '"; z:"'+ row[zvar.currentIndex] +'"}'
    });
    values = values.substring(1);
    var model = 'ListModel{id:"'+key+'"; '+values+'}'
    var text = 'import QtQuick 2.0;import QtDataVisualization 1.2;Scatter3DSeries{id:activeSeries;baseColor:"'+color+'";ItemModelScatterDataProxy{id: dataProxy_'+key+';itemModel:'+key+'; xPosRole:"x"; yPosRole:"y"; zPosRole:"z"}'+model+'}';
    var series = Qt.createQmlObject(text, scatterStub, 'dynSeries['+key+']');
    removeSeries(key);
    visualization3d.series[key] = {"raw": series, "data": data, "color": color};
    scatter3d.addSeries(series);
  }

  function removeSeries(key){
    if(key in visualization3d.series){
      scatter3d.removeSeries(visualization3d.series[key].raw);
    }
  }

  function update(){
    for(var key in series){
      addSeries(key, series[key].data, series[key].color);
    }
  }

  RowLayout {
    anchors.fill: parent
    anchors.margins: 10
    ColumnLayout {
      id: scatterStub
      Scatter3D {
        id: scatter3d
        anchors.fill: parent
        orthoProjection: true
      }
    }
    GroupBox {
      id: buttons
      title: "Variables"
      anchors.top: parent.top
      GridLayout{
        columns: 2
      Label {
        text: "x-variable: "
      }
      ComboBox {
        id: xvar
        objectName: "xvarComboBox"
        model: ["none"]
        onCurrentIndexChanged: visualization3d.update()
      }
      Label {
        text: "y-variable: "
      }
      ComboBox {
        id: yvar
        objectName: "yvarComboBox"
        model: ["none"]
        onCurrentIndexChanged: visualization3d.update()
      }
      Label {
        text: "z-variable: "
      }
      ComboBox {
        id: zvar
        objectName: "zvarComboBox"
        model: ["none"]
        onCurrentIndexChanged: visualization3d.update()
      }
    }
    }
  }
}

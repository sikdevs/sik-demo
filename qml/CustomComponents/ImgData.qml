import QtQuick 2.7
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

ColumnLayout{
  signal imgFileSelected(var qurl)
  property bool selected
  Button{
    text: "select"
    onClicked: fileDialog.visible = true
  }
  FileDialog {
    id: fileDialog
    title: "Please choose a folder"
    folder: shortcuts.home
	selectFolder: true
    onAccepted: {
      selected=true
      imgFileSelected(Qt.resolvedUrl(this.fileUrl))
    }
  }
}

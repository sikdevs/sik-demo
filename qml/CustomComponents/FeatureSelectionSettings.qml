import QtQuick 2.7
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

import "." as Components

Item{
  id: featureSettings
  objectName: "featureSettings"

  signal dataFileSelected(var qurl, var header)
  signal costFileSelected(var qurl, var header)
  signal onTabChanged(var obj, var visible)
  signal run(var data)

  function stringifySettings(){
    var settings = {};
    return JSON.stringify(settings);
  }
  function clear(){
    featureListView.model.clear();
  }
  function addSelection(selection, merit, meanCost, costScatter){
    var sel = {"scatter": costScatter, "mer": merit, "mean": meanCost, "selected": selection};
    featureListView.model.append(sel);
  }

  anchors.fill:parent
  anchors.margins: 10

  ColumnLayout{
    anchors.fill: parent
    GridLayout{
      columns: 3
      GroupBox{
        title: "Data"
        anchors.top: parent.top
        Components.CsvData{
          onCsvFileSelected: featureSettings.dataFileSelected(qurl, header)
        }
      }
      GroupBox{
        title: "Costs"
        anchors.top: parent.top
        Components.CsvData{
          onCsvFileSelected: featureSettings.costFileSelected(qurl, header)
        }
      }
      GroupBox{
        title: "Settings"
        anchors.top: parent.top
        GridLayout{
          columns: 2
          Label{text: "num Candidates: "}
          TextField{id: numCandidates; text: "40"; validator: intsOnly}
          Label{text: "n iterations: "}
          TextField{id: nIterations; text: "100"; validator: intsOnly}
          Label {
            id: classColL
            objectName: "classColLabel"
            text: "Class-Row: "
          }
          ComboBox {
            id: featureClassesCB
            objectName: "featureClassesCB"
            model: ["none"]
          }
        }
      }
      Button{
        id: "runFeatureSettings"
        objectName: "runFeatureSettings"
        text: "Run"
        onClicked:{
          featureSettings.run(stringifySettings());
        }
      }
      }

       ScrollView{
        id: featureScrollView
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignTop
        flickableItem.interactive: true
        contentItem: 
        ListView{
          id: featureListView;
          objectName: "featureListView";
          model: featureLstModel
          delegate: Components.Selection{
            selection: selected;
            merit: mer;
            meanCost: mean;
            costScatter: scatter;
          }
        }
      }
    ListModel {
      id: featureLstModel;
      //ListElement{scatter: 1; mer: 0; mean: 0.5; selected: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"}
      //ListElement{scatter: 2; mer: 9; mean: 0.5; selected: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"}
      //ListElement{scatter: 4; mer: 6; mean: 0.5; selected: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"}
      //ListElement{scatter: 4; mer: 3; mean: 0.5; selected: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"}
      //ListElement{scatter: 3; mer: 2; mean: 0.5; selected: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"}
    }


    RegExpValidator{
      id: intsOnly
      regExp: /[1-9]{1}[0-9]{0,10}/
    }
  }
}

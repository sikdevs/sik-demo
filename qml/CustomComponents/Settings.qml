import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0
import "." as Components

StackLayout{
  id: stackLayout
  signal csvFileSelected(var qurl)
  signal imgFileSelected(var qurl)
  anchors.fill: parent
  states: [
    State{
      name: "Cluster"
      when: clusterMenuItem.checked
      PropertyChanges{target: stackLayout; currentIndex: 0}
    },
    State{
      name: "Unigrams"
      when: unigramsMenuItem.checked
      PropertyChanges{target: stackLayout; currentIndex: 1}
    },
    State{
      name: "Trees"
      when: treesMenuItem.checked
      PropertyChanges{target: stackLayout; currentIndex: 2}
    },
    State{
        name: "FeatureSelection"
        when: featureselectionMenuItem.checked
        PropertyChanges{target: stackLayout; currentIndex: 3}
      }
    ]
    Components.ClusterSettings {
      onCsvFileSelected: stackLayout.csvFileSelected(qurl)
      objectName: "clusterSettings"
    }
    Components.BovSettings {
      onImgFileSelected: stackLayout.imgFileSelected(qurl)
      objectName: "bovSettings"
    }
    Components.TreeSettings {
      onCsvFileSelected: stackLayout.csvFileSelected(qurl)
      objectName: "treeSettings"
    }
    Components.FeatureSelectionSettings {
      objectName: "featureSettings"
    }
  }

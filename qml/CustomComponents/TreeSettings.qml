import QtQuick 2.7
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0

import "." as Components

Item{
  id: treeSettings
  signal csvFileSelected(var qurl, var header)
  signal run(var data)
  function stringifySettings(){
    var settings = {};
    settings.mode = dtRadioButton.checked ? "DT" : "GMT";
    settings.maxDepth = parseInt(maxDepthTF.text);
    settings.minElements = parseInt(minElementsTF.text);
    settings.minImpurity = parseFloat(minImpurityTF.text);
    settings.rho = parseFloat(rhoTF.text);
    settings.nClusters = parseInt(nClustersTF.text);
    settings.prune = pruneRB.checked;
    settings.sigmaMeanStd = parseFloat(meanStdTF.text);
    settings.sigmaMahalanobis = parseFloat(mahalanobisTF.text);
    settings.classColIndex = classColComboBox.currentIndex;
    settings.rejectOption = {};
    settings.rejectOption.none = noneRB.checked;
    settings.rejectOption.minmax = minMaxRB.checked;
    settings.rejectOption.meanstd = meanStdRB.checked;
    settings.rejectOption.mahalanobis = mahalanobisRB.checked;
    return JSON.stringify(settings);
  }
  function setResults(hits, misses){
    hitsVL.text = "" + hits;
    missesVL.text = "" + misses;
  }
  anchors.fill: parent
  anchors.margins: 10
  states: [
    State{
      name: "DT"
      when: dtRB.checked
      PropertyChanges{target: rejectOptionL; enabled: true}
      PropertyChanges{target: rejectOptionLayout; enabled: true}
      PropertyChanges{target: pruneL; enabled: true}
      PropertyChanges{target: pruneRB; enabled: true}
      PropertyChanges{target: nClustersL; enabled: false}
      PropertyChanges{target: nClustersTF; enabled: false}
      PropertyChanges{target: rhoL; enabled: false}
      PropertyChanges{target: rhoTF; enabled: false}
      PropertyChanges{target: classColL; enabled: true}
      PropertyChanges{target: classColComboBox; enabled: true}
    },
    State{
      name: "GMT"
      when: gmtRadioButton.checked
      PropertyChanges{target: rejectOptionL; enabled: false}
      PropertyChanges{target: rejectOptionLayout; enabled: false}
      PropertyChanges{target: pruneL; enabled: false}
      PropertyChanges{target: pruneRB; enabled: false}
      PropertyChanges{target: nClustersL; enabled: true}
      PropertyChanges{target: nClustersTF; enabled: true}
      PropertyChanges{target: rhoL; enabled: true}
      PropertyChanges{target: rhoTF; enabled: true}
      PropertyChanges{target: classColL; enabled: false}
      PropertyChanges{target: classColComboBox; enabled: false}
    }
  ]
  ColumnLayout{
  GridLayout{
    columns: 3
    GroupBox{
      title: "Tree Mode"
      anchors.top: parent.top
      ColumnLayout{
        ExclusiveGroup { id: treeMode }
        RadioButton {
          text: "Decision Tree"
          id: dtRadioButton
          checked: true
          exclusiveGroup: treeMode
        }
        RadioButton {
          text: "Gaussian Mixture Tree"
          id: gmtRadioButton
          exclusiveGroup: treeMode
        }
      }
    }
    GroupBox{
      title: "Data"
      anchors.top: parent.top
      Components.CsvData{
        onCsvFileSelected: treeSettings.csvFileSelected(qurl, header)
      }
    }
    GroupBox{
      title: "Settings"
      anchors.top: parent.top
      GridLayout{
        columns: 2
        Label{id: maxDepthL; text: "max depth: "}
        TextField{id: maxDepthTF; text: "10"; validator: intsOnly}
        Label{id: minElementsL; text: "min elements: "}
        TextField{id: minElementsTF; text: "10"; validator: intsOnly}
        Label{id: minImpurityL; text: "min impurity gain: "}
        TextField{id: minImpurityTF; text: "0.1"; validator: numbersOnly}


        Label{id: rhoL; text: "rho: "; enabled: gmtRadioButton.checked}
        TextField{id: rhoTF; text: "0.85"; validator: numbersOnly; enabled: gmtRadioButton.checked}
        Label{id: nClustersL; text: "n Clusters: "; enabled: gmtRadioButton.checked}
        TextField{id: nClustersTF; text: "2"; validator: intsOnly; enabled: gmtRadioButton.checked}
        
        Label{id: pruneL; text: "prune: "}
        CheckBox{id: pruneRB;}
        Label{id: rejectOptionL; text: "reject Option: "}
        GridLayout{
          id: rejectOptionLayout
          columns: 2
          ExclusiveGroup {id: rejectMode}
          RadioButton{
            text: "None"
            id: noneRB
            checked: true
            exclusiveGroup: rejectMode
          }
          RadioButton{
            text: "Min & Max"
            id: minMaxRB
            exclusiveGroup: rejectMode
          }
          RadioButton{
            text: "Mean & Std"
            id: meanStdRB
            exclusiveGroup: rejectMode
          }
          RadioButton{
            text: "Mahalanobis"
            id: mahalanobisRB
            exclusiveGroup: rejectMode
          }
        }
          
          Label{id: meanStdL; enabled: meanStdRB.checked; text: "Mean & Std sigma: "}
          TextField{id: meanStdTF; enabled: meanStdRB.checked; text: "2.5"; validator: numbersOnly}
          Label{id: mahalanobisL; enabled: mahalanobisRB.checked; text: "Mahalanobis sigma: "}
          TextField{id: mahalanobisTF; enabled: mahalanobisRB.checked; text: "2.5"; validator: numbersOnly}
      Label {
        id: classColL
        objectName: "classColLabel"
        text: "Class-Row: "
      }
      ComboBox {
        id: classColComboBox
        objectName: "classColComboBox"
        model: ["none"]
      }
      }
    }
      Button{
        id: "runTreeSettings"
        objectName: "runTreeSettings"
        text: "Run"
        onClicked:{
          treeSettings.run(stringifySettings());
        }
      }
    }
      GridLayout{
        columns: 4;
        Label{
          text: "Hits: "
        }
        Label{
          id: hitsVL
          objectName: "hitsVL"
          text: ""
        }
        Label{
          text: "Misses: "
        }
        Label{
          id: missesVL
          objectName: "missesVL"
          text: ""
        }
      }
    RegExpValidator{
      id: jsonArrayOnly
      regExp: /\[(\[(-?(?=[1-9]|0(?!\d))\d+(\.\d+)?([eE][+-]?\d+)?,?)*\],?)*\]/
    }
    RegExpValidator{
      id: intsOnly
      regExp: /[1-9]{1}[0-9]{0,10}/
    }
    DoubleValidator{
      id: numbersOnly
    }
  }
}

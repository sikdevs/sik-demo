import QtQuick 2.7
import QtQuick.Layouts 1.3

GridLayout {
  property string src: "";
  property int imageSize: 128;
  property variant histogram: [];
  width: 3 * imageSize;
  height: imageSize;
  Item{
    width: imageSize;
    height: imageSize;
    Image{
      id: image
      width: imageSize;
      height: imageSize;
      fillMode: Image.Stretch;
      source: src;
    }
  }
  Canvas {
    id: canvas
    width: imageSize * 2;
    height: imageSize;
    onPaint:{
      var ctx = this.getContext('2d');
      ctx.strokeStyle = Qt.rgba(0, 0, 0, 1);
      ctx.lineWidth = 1;
      ctx.beginPath();
      var max = Math.max.apply(null,histogram)
      ctx.moveTo(0,0);
      for(var i = 0; i < histogram.length; ++i){
        ctx.moveTo(i, imageSize);
        ctx.lineTo(i, imageSize - (imageSize * (histogram[i]/max)));
      }
      ctx.stroke();
    }
  }
  Rectangle{
    id: debugRectangle
    anchors.fill: parent
    color: "transparent"
    border.width: 1
    border.color: "transparent"
  }
}

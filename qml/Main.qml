import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.0
import "./CustomComponents" as Components

ApplicationWindow {
  id: window
  visible: true
  width: 800
  height: 600
  signal csvFileSelected(var qurl)

  ExclusiveGroup{
    id: exclusiveGroup
  }
  menuBar: MenuBar{
    Menu {
      title: "File"
      MenuItem {
        text: "Exit"
        shortcut: StandardKey.Quit
        onTriggered: Qt.quit()
      }
    }
    Menu {
      title: "Mode"
      MenuItem {
        id: clusterMenuItem
        text: "Clusters"
        checkable: true
        exclusiveGroup: exclusiveGroup
      }
      MenuItem {
        id: unigramsMenuItem
        text: "Unigrams"
        checkable: true
        checked: true
        exclusiveGroup: exclusiveGroup
      }
      MenuItem {
        id: featureselectionMenuItem
        text: "Featureselection"
        checkable: true
        exclusiveGroup: exclusiveGroup
      }
      MenuItem {
        id: treesMenuItem
        text: "Trees"
        checkable: true
        exclusiveGroup: exclusiveGroup
      }
    }
  }
  statusBar: StatusBar{
    height: 20
    RowLayout {
      anchors.fill: parent
      BusyIndicator {
        id: busyIndicator
        objectName: "busyIndicator"
        anchors {
          top: parent.top
          bottom: parent.bottom
        }
        running: false
      }
      Label {

        id: statusLabel
        objectName: "statusLabel"
        text: "Idle"
        function setStatus(msg, showSpinner){
          if(msg == "Idle" || !showSpinner){
            busyIndicator.running = false;
          }else{
            busyIndicator.running = true;
          }
          statusLabel.text = msg;
        }

        anchors {
          top: parent.top
          bottom: parent.bottom
          left: busyIndicator.right
        }
      }
    }
  }
  Components.Settings{
    objectName: "Settings"
    onCsvFileSelected: window.csvFileSelected(qurl)
  }

}

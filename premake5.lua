require( "contrib.premake-qt.qt" )
local qt = premake.extensions.qt

workspace "SIK"
  configurations {"Debug", "Release"}

  includedirs{
    "include",
      "../libsik/include",
      "../libsik/contrib/eigen",
  }

  warnings "Extra"
  flags {"C++11"}

  targetdir "build/bin/%{cfg.buildcfg}"
  objdir "build/obj/%{cfg.buildcfg}"

project "SIK-Demo"
  language "C++"
  kind "WindowedApp"
  links{
    "turbojpeg", "SIKlib"
  }
  files {
    "include/**.h",
      "src/**.cpp",
      "src/**.h",
      "src/**.ui"
  }
-- Enable Qt for this project.
  qt.enable()
-- change the following two lines accordingly to your QT configuration
  qtpath "/usr"
  qtincludepath "/usr/include/qt"
  pic "on"
  qtmodules { "core", "gui", "widgets", "quick", "qml", "datavisualization"}
  qtprefix "Qt5"

configuration "Debug" 
  symbols "On"
configuration "Release"
  flags {"Optimize"}
